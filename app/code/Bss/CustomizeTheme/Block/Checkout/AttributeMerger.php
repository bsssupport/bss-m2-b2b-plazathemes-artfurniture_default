<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CustomizeTheme
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomizeTheme\Block\Checkout;

/**
 * Class AttributeMerger
 *
 * @package Bss\CustomizeTheme\Block\Checkout
 */
class AttributeMerger extends \Magento\Checkout\Block\Checkout\AttributeMerger
{
    /**
     * AttributeMerger constructor.
     * @param \Magento\Customer\Helper\Address $addressHelper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Directory\Helper\Data $directoryHelper
     */
    public function __construct(
        \Magento\Customer\Helper\Address $addressHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Directory\Helper\Data $directoryHelper
    )
    {
        parent::__construct(
            $addressHelper,
            $customerSession,
            $customerRepository,
            $directoryHelper
        );
    }

    /**
     * Retrieve UI field configuration for given attribute
     *
     * @param string $attributeCode
     * @param array $attributeConfig
     * @param array $additionalConfig field configuration provided via layout XML
     * @param string $providerName name of the storage container used by UI component
     * @param string $dataScopePrefix
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function getFieldConfig(
        $attributeCode,
        array $attributeConfig,
        array $additionalConfig,
        $providerName,
        $dataScopePrefix
    ) {
        $allow = ['suffix', 'prefix'];

        // street attribute is unique in terms of configuration, so it has its own configuration builder
        if (isset($attributeConfig['validation']['input_validation'])) {
            $validationRule = $attributeConfig['validation']['input_validation'];
            $attributeConfig['validation'][$this->inputValidationMap[$validationRule]] = true;
            unset($attributeConfig['validation']['input_validation']);
        }

        if ($attributeConfig['formElement'] == 'multiline') {
            return $this->getMultilineFieldConfig($attributeCode, $attributeConfig, $providerName, $dataScopePrefix);
        }

        $uiComponent = isset($this->formElementMap[$attributeConfig['formElement']])
            ? $this->formElementMap[$attributeConfig['formElement']]
            : 'Magento_Ui/js/form/element/abstract';
        $elementTemplate = isset($this->templateMap[$attributeConfig['formElement']])
            ? 'ui/form/element/' . $this->templateMap[$attributeConfig['formElement']]
            : 'ui/form/element/' . $attributeConfig['formElement'];

        if (in_array($attributeCode, $allow)) {
            $element = [
                'component' => isset($additionalConfig['component']) ? $additionalConfig['component'] : $uiComponent,
                'config' => $this->mergeConfigurationNode(
                    'config',
                    $additionalConfig,
                    [
                        'config' => [
                            // customScope is used to group elements within a single
                            // form (e.g. they can be validated separately)
                            'customScope' => $dataScopePrefix,
                            'template' => 'ui/form/field',
                            'elementTmpl' => $elementTemplate,
                        ],
                    ]
                ),
                'dataScope' => $dataScopePrefix . '.' . $attributeCode,
                'label' => $attributeConfig['label'],
                'provider' => $providerName,
                'sortOrder' => isset($additionalConfig['sortOrder'])
                    ? $additionalConfig['sortOrder']
                    : $attributeConfig['sortOrder'],
                'validation' => $this->mergeConfigurationNode('validation', $additionalConfig, $attributeConfig),
                'filterBy' => isset($additionalConfig['filterBy']) ? $additionalConfig['filterBy'] : null,
                'customEntry' => isset($additionalConfig['customEntry']) ? $additionalConfig['customEntry'] : null,
                'visible' => isset($additionalConfig['visible']) ? $additionalConfig['visible'] : true,
            ];
        } else {
            $element = [
                'component' => isset($additionalConfig['component']) ? $additionalConfig['component'] : $uiComponent,
                'config' => $this->mergeConfigurationNode(
                    'config',
                    $additionalConfig,
                    [
                        'config' => [
                            // customScope is used to group elements within a single
                            // form (e.g. they can be validated separately)
                            'customScope' => $dataScopePrefix,
                            'template' => 'ui/form/field',
                            'elementTmpl' => $elementTemplate,
                        ],
                    ]
                ),
                'dataScope' => $dataScopePrefix . '.' . $attributeCode,
                'label' => $attributeConfig['label'],
                'provider' => $providerName,
                'sortOrder' => isset($additionalConfig['sortOrder'])
                    ? $additionalConfig['sortOrder']
                    : $attributeConfig['sortOrder'],
                'validation' => $this->mergeConfigurationNode('validation', $additionalConfig, $attributeConfig),
                'options' => $this->getFieldOptions($attributeCode, $attributeConfig),
                'filterBy' => isset($additionalConfig['filterBy']) ? $additionalConfig['filterBy'] : null,
                'customEntry' => isset($additionalConfig['customEntry']) ? $additionalConfig['customEntry'] : null,
                'visible' => isset($additionalConfig['visible']) ? $additionalConfig['visible'] : true,
            ];
        }

        if ($attributeCode === 'region_id' || $attributeCode === 'country_id') {
            unset($element['options']);
            $element['deps'] = [$providerName];
            $element['imports'] = [
                'initialOptions' => 'index = ' . $providerName . ':dictionaries.' . $attributeCode,
                'setOptions' => 'index = ' . $providerName . ':dictionaries.' . $attributeCode
            ];
        }

        if (isset($attributeConfig['value']) && $attributeConfig['value'] != null) {
            $element['value'] = $attributeConfig['value'];
        } elseif (isset($attributeConfig['default']) && $attributeConfig['default'] != null) {
            $element['value'] = $attributeConfig['default'];
        } else {
            $defaultValue = $this->getDefaultValue($attributeCode);
            if (null !== $defaultValue) {
                $element['value'] = $defaultValue;
            }
        }
        return $element;
    }
}