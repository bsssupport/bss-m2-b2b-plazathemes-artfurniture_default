<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_SalesRep
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomizeTheme\Plugin\Block;

use Bss\CatalogPermission\Helper\Data;
use Bss\CatalogPermission\Helper\ModuleConfig;
use Magento\Customer\Model\Session;

/**
 * Class Menu
 *
 * @package Bss\CustomizeTheme\Plugin\Block
 */
class Menu
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ModuleConfig
     */
    protected $moduleConfig;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * Menu constructor.
     *
     * @param Session $customerSession
     * @param ModuleConfig $moduleConfig
     * @param Data $helperData
     */
    public function __construct(
        Session $customerSession,
        ModuleConfig $moduleConfig,
        Data $helperData
    ) {
        $this->customerSession = $customerSession;
        $this->moduleConfig = $moduleConfig;
        $this->helperData = $helperData;
    }

    /**
     * @param object $subject
     * @param string $result
     * @param null $category
     * @param int $level
     * @param false $last
     * @param null $item
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterDrawCustomMenuItem($subject, $result, $category= null, $level = 0, $last = false, $item= null)
    {
        if ($this->moduleConfig->enableCatalogPermission() && $this->moduleConfig->disableCategoryLink()) {
            $customerGroupId = $this->customerSession->getCustomerGroupId();
            $listIdSubCategory = $this->helperData->getIdCategoryByCustomerGroupIdDisableInCmsPage($customerGroupId);
            if (in_array($category, $listIdSubCategory)) {
                return '';
            }
            return $result;
        }
        return $result;

    }
}